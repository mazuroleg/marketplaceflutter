import 'dart:convert';

import 'package:flutter_application/models/product.dart';
import 'package:http/http.dart' as http;

class RemoteServices {
  static var client = http.Client();
  static Future<List<Product>> fetchProducts() async {
    var response = await client.get(
        Uri.parse('https://bsa-marketplace.azurewebsites.net/api/Products'));
    if (response.statusCode == 200) {
      var jsonString = response.body;
      return productFromJson(jsonString);
    } else {
      //show error message
      throw Exception('error');
    }
  }
}
