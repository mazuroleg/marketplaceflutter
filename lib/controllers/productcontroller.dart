import 'package:flutter_application/models/product.dart';
import 'package:flutter_application/services/remote_services.dart';
import 'package:get/state_manager.dart';

class ProductController extends GetxController {
  var isLoading = true.obs;
  var productList = <Product>[].obs;
  @override
  void onInit() {
    fetchProduduct();
    super.onInit();
  }

  void fetchProduduct() async {
    isLoading(true);
    try {
      var products = await RemoteServices.fetchProducts();
      if (products != null) {
        productList.value = products;
      }
    } finally {
      isLoading(false);
    }
  }
}
